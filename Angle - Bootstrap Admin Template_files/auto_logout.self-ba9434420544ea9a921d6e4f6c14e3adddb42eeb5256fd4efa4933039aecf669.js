var inactivity_timeout;
$(document).ready(function() {
  var ask_to_logout = function() {
    var r = confirm("We are going to log you out due to inactivity");
    if (r == true) {
      $("#logout-button").click();
    }
  };
  $(window).blur(function(e) {
    inactivity_timeout = setTimeout(ask_to_logout, 600000);
  });

  $(window).focus(function(e) {
    clearTimeout(inactivity_timeout);
  });
});
