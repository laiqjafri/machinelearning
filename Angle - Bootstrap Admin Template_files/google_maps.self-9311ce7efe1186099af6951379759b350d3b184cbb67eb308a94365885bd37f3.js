var init_map = function() {
  mark_locations();
}

var mark_locations = function() {
  if (typeof(user_locations) == "undefined" || user_locations.length == 0) {
    return false;
  }
  var map = new google.maps.Map(document.getElementById('locations-map'), {
    zoom: 10,
      center: new google.maps.LatLng(user_locations[0][1], user_locations[0][2]),
      mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  var infowindow = new google.maps.InfoWindow();

  var marker, i;

  for (i = 0; i < user_locations.length; i++) {
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(user_locations[i][1], user_locations[i][2]),
      map: map
    });

    google.maps.event.addListener(marker, 'click', (function(marker, i) {
      return function() {
        infowindow.setContent(user_locations[i][0]);
        infowindow.open(map, marker);
      }
    })(marker, i));
  }
}
