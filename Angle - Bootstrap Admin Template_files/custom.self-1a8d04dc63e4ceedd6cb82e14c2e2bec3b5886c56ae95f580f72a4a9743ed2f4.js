// Custom jQuery
// ----------------------------------- 


(function(window, document, $, undefined){

  $(function(){
    $(".rwl-autocomplete").each(function(i, item) {
      var url = $(item).data('href');
      var key = $(item).data('key');
      $(item).typeahead({
        remote: {
                  url : url + '.json?query=%QUERY'
                },
        template: '<p style="color:#000;">{{' + key + ' }}</p>',
        engine: Hogan
      }).on('typeahead:selected', function($e, datum){
        location.href = datum['url'];
      });
    });

    $('.rwl-data-table').dataTable({
      "bInfo" : false
    });

    if($("#line-chart-social").length > 0) {
      draw_line_chart($("#line-chart-social"), social_data);
    }

    if($("#pie-chart-social").length > 0) {
      draw_pie_chart($("#pie-chart-social"), social_pie_data);
    }

    if($("#stack-chart-social").length > 0) {
      draw_stack_chart($("#stack-chart-social"), social_stack_data);
    }

    if($("#spline-chart").length > 0) {
      draw_spline_chart($("#spline-chart"), spline_chart_data);
    }

    if($(".social-form-date").length > 0) {
      $(".social-form-date").datetimepicker({
        format: 'YYYY-MM-DD'
      });
    }

    if($(".export-social-data-csv").length > 0) {
      $(".export-social-data-csv").click(function(e) {
        e.preventDefault();
        $.ajax({
          url: '/user/social/export?' + $("#social-export-form").serialize(),
          data: {},
          dataType: 'csv',
          success: function(data) {
          }
        });
      });
    }
  });
})(window, document, window.jQuery);

function draw_pie_chart(obj, data) {
  var options = { 
    series: {
              pie: {
                     show: true,
                     innerRadius: 0,
                     label: {
                       show: true,
                       radius: 0.8,
                       formatter: function (label, series) {
                         return '<div class="flot-pie-label">' +
                           //label + ' : ' +
                           Math.round(series.percent) +
                           '%</div>';
                       },
                       background: {
                                     opacity: 0.8,
                                     color: '#222'
                                   }
                     }
                   }
            }
  };

  $.plot(obj, data, options);
}


function draw_line_chart(obj, data) {
  var options = {
    series: {
              lines: {
                       show: true,
                       fill: 0.01
                     },
              points: {
                        show: true,
                        radius: 4
                      }
            },
    grid: {
            borderColor: '#eee',
            borderWidth: 1,
            hoverable: true,
            backgroundColor: '#fcfcfc'
          },
    tooltip: true,
    tooltipOpts: {
      content: function (label, x, y) { return x + ' : ' + y; }
    },
    xaxis: {
             tickColor: '#eee',
             mode: 'categories'
           },
    yaxis: {
             // position: 'right' or 'left'
             tickColor: '#eee'
           },
    shadowSize: 0
  };
  $.plot(obj, data, options);
}

function draw_stack_chart(obj, data) {
  var options = { 
    series: {
              stack: true,
              bars: {
                align: 'center',
                lineWidth: 0,
                show: true,
                barWidth: 0.6,
                fill: 0.9 
              }   
            },  
    grid: {
            borderColor: '#eee',
            borderWidth: 1,
            hoverable: true,
            backgroundColor: '#fcfcfc'
          },  
    tooltip: true,
    tooltipOpts: {
      content: function (label, x, y) { return x + ' : ' + y; }
    },  
    xaxis: {
             tickColor: '#fcfcfc',
             mode: 'categories'
           },  
    yaxis: {
             // position: 'right' or 'left'
             tickColor: '#eee'
           },  
    shadowSize: 0
  };
  $.plot(obj, data, options);
}

function draw_spline_chart(obj, data) {
  var options = {
    series: {
              lines: {
                       show: false
                     },
              points: {
                        show: true,
                        radius: 4
                      },
              splines: {
                         show: true,
                         tension: 0.4,
                         lineWidth: 1,
                         fill: 0.5
                       }
            },
    grid: {
            borderColor: '#eee',
            borderWidth: 1,
            hoverable: true,
            backgroundColor: '#fcfcfc'
          },
    tooltip: true,
    tooltipOpts: {
      content: function (label, x, y) { return x + ' : ' + y; }
    },
    xaxis: {
             tickColor: '#fcfcfc',
             mode: 'categories'
           },
    yaxis: {
             min: 0,
             max: 150, // optional: use it for a clear represetation
             tickColor: '#eee',
             //position: 'right' or 'left',
             tickFormatter: function (v) {
               return v/* + ' visitors'*/;
             }
           },
    shadowSize: 0
  };
  console.log(data);
    var data3 = [{
      "label": "Home",
      "color": "#1ba3cd",
      "data": [
        ["1", 38],
        ["2", 40],
        ["3", 42],
        ["4", 48],
        ["5", 50],
        ["6", 70],
        ["7", 145],
        ["8", 70],
        ["9", 59],
        ["10", 48],
        ["11", 38],
        ["12", 29],
        ["13", 30],
        ["14", 22],
        ["15", 28]
      ]
    }];
    console.log(data3);
  $.plot(obj, data, options);
}
