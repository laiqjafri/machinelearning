predict_usps = function(k) {
  usps = read.table('USPS.txt')
  train <- sample(1:nrow(usps), 8000)
  
  train.x = usps[train, 1:256]
  train.y = usps[train, 257:257]

  test.x = usps[-train, 1:256]
  test.y = usps[-train, 257:257]

  pred.y = myknn(train.x, train.y, test.x, test.y, k)
}
